import { Component, ViewChild } from '@angular/core';
import { Platform, App, Events, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LocalStorage } from '../components/local-storage/local-storage';

import { AuthService } from '../providers/auth/auth';

import { MenuPage } from '../pages/menu/menu';
import { WelcomePage } from '../pages/welcome/welcome';
import * as moment from 'moment';
import { AdminPage } from '../pages/admin/admin';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  public rootPage: any = WelcomePage;

  @ViewChild(Nav) nav: Nav;

  constructor(
    public app: App,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public events: Events,
    public auth: AuthService,
    public localStorage: LocalStorage,
  ) {
    moment.locale('es');
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.init();
    }).then(() => this.auth.init());
  }

  init() {
    this.events.subscribe('auth:ready', () => {
      if (this.auth.isLoggedIn()) {
        this.localStorage.get('role')
          .then((role) => {
            if (role == 'admin') {
              this.goAdmin();
            } else {
              this.goMenu();
            }
          })
      } else {
        this.showWelcome();
      }
    });
  }

  goAdmin() {
    this.nav.setRoot(AdminPage);
  }

  goMenu() {
    this.nav.setRoot(MenuPage);
  }

  showWelcome() {
    this.nav.setRoot(WelcomePage);
  }
}
