import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MyHttpInterceptor } from '../providers/interceptor/interceptor';

import { AuthService } from '../providers/auth/auth';
import { AttachmentService } from '../providers/attachment/attachment';
import { JustService } from '../providers/justificante/justificante'

import { LocalStorage } from '../components/local-storage/local-storage';
import { NativeStorage } from '@ionic-native/native-storage';

import { AdminPage } from '../pages/admin/admin';
import { AdminHistoryPage } from '../pages/admin-history/admin-history';
import { AdminPendingPage } from '../pages/admin-pending/admin-pending';

import { SubmitPage } from '../pages/submit/submit';
import { HistoryPage } from '../pages/history/history';
import { RegisterPage } from '../pages/register/register';

import { LoginPage } from '../pages/login/login'
import { MenuPage } from '../pages/menu/menu';
import { WelcomePage } from '../pages/welcome/welcome'

import { JustDetailsPage } from '../pages/just-details/just-details';
import { AttachmentDetailPage } from '../pages/attachment-detail/attachment-detail';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { File } from '@ionic-native/file';

@NgModule({
  declarations: [
    MyApp,
    SubmitPage,
    HistoryPage,
    LoginPage,
    MenuPage,
    WelcomePage,
    RegisterPage,
    AdminPage,
    AdminHistoryPage,
    AdminPendingPage,
    JustDetailsPage,
    AttachmentDetailPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SubmitPage,
    HistoryPage,
    LoginPage,
    MenuPage,
    WelcomePage,
    RegisterPage,
    AdminPage,
    AdminHistoryPage,
    AdminPendingPage,
    JustDetailsPage,
    AttachmentDetailPage
  ],
  providers: [
    AuthService,
    AttachmentService,
    JustService,
    StatusBar,
    SplashScreen,
    LocalStorage,
    NativeStorage,
    File,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MyHttpInterceptor,
      multi: true,
    },
  ]
})
export class AppModule { }
