import { Injectable, NgZone } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Events } from 'ionic-angular';
import { LocalStorage } from '../../components/local-storage/local-storage';

// Global variable, should be available in scope
declare var justServer: any;

const USER_KEY = 'user_id';
const EMAIL_KEY = 'email';
const PROFILE_KEY = 'profile_id';
const TOKEN_KEY = 'token';
const REFRESH_TOKEN_KEY = 'refresh_token';
const USER_NAME = 'name';
const USER_LAST_NAME = 'last_name';
const MATRICULA = 'matricula';
const ROLE = 'role';

@Injectable()
export class AuthService {
  baseUrl: string = justServer.protocol +
    '://' +
    justServer.url +
    justServer.port +
    '/api/v1';
  headers: Headers = new Headers();
  zoneImpl: NgZone;

  email: any = null;
  token: any = null;
  refresh_token: any = null;
  profile_id: any = null;
  user_id: any = null;
  user_name: any = null;
  user_last_name: any = null;
  matricula: any = null;
  role: any = null;

  logged_in: boolean = false;
  ready: boolean = false;

  constructor(
    public http: Http,
    zone: NgZone,
    public events: Events,
    public local: LocalStorage
  ) {
    this.zoneImpl = zone;
    this.headers.append('Content-Type', 'application/json');
  }

  public init(): Promise<any> {
    return this.local
      .get(EMAIL_KEY)
      .then(email => {
        this.email = email;
      })
      .then(() => this.local.get(USER_KEY))
      .then(user => {
        if (user == null) throw 'logout';
        this.user_id = user;
      })
      .then(() => this.local.get(PROFILE_KEY))
      .then(profile => {
        if (profile == null) throw 'logout';
        this.profile_id = profile;
      })
      .then(() => this.local.get(USER_NAME))
      .then(name => {
        if (name == null) throw 'logout';
        this.user_name = name;
      })
      .then(() => this.local.get(USER_LAST_NAME))
      .then(last_name => {
        if (last_name == null) throw 'logout';
        this.user_last_name = last_name;
      })
      .then(() => this.local.get(MATRICULA))
      .then(matricula => {
        if (matricula == null) throw 'logout';
        this.matricula = matricula;
      })
      .then(() => this.local.get(ROLE))
      .then(role => {
        if (role = null) throw 'logout';
        this.role = role;
      })
      .then(() => this.local.get(TOKEN_KEY))
      .then(token => {
        if (token == null) throw 'logout';
        this.token = token;

        this.logged_in = true;
        this.ready = true;
        this.events.publish('auth:ready');
      })
      .catch(error => {
        if (error == 'logout') return this.logout();
      });
  }

  public setCredentials(credentials): Promise<any> {
    this.token = credentials.token;
    this.refresh_token = credentials.refresh_token.token;
    this.email = credentials.user.email;
    this.profile_id = credentials.profile.id;
    this.user_id = credentials.user.id;
    this.user_name = credentials.user.name;
    this.user_last_name = credentials.user.last_name;
    this.matricula = credentials.user.matricula;
    this.role = credentials.user.role;
    return Promise.all([
      this.local.set(TOKEN_KEY, this.token),
      this.local.set(REFRESH_TOKEN_KEY, this.refresh_token),
      this.local.set(EMAIL_KEY, this.email),
      this.local.set(PROFILE_KEY, this.profile_id),
      this.local.set(USER_KEY, this.user_id),
      this.local.set(USER_NAME, this.user_name),
      this.local.set(USER_LAST_NAME, this.user_last_name),
      this.local.set(MATRICULA, this.matricula),
      this.local.set(ROLE, this.role)
    ]).then(() => {
      this.events.publish('auth:ready');
    });
  }

  public updateTokens(credentials): Promise<any> {
    this.token = credentials.token;
    this.refresh_token = credentials.refresh_token.token;
    return Promise.all([
      this.local.set(TOKEN_KEY, this.token),
      this.local.set(REFRESH_TOKEN_KEY, this.refresh_token)
    ]).then(() => {
      this.events.publish('auth:ready');
    });
  }

  public login(email: string, password: string) {
    return this.http
      .post(`${this.baseUrl}/auth/login`,
        { email: email, password: password },
        { headers: this.headers })
      .map((res: Response) => res.json())
      .toPromise()
      .then((res) => {
        this.setCredentials(res)
          .then(() => this.logged_in = true)
        return Promise.resolve(res);
      })
      .catch((err) => {
        this.logged_in = false
        return Promise.reject(err);
      })
  }

  public recover(email) {
    const body = {
      email: email,
    };
    let headers: Headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(`${this.baseUrl}/auth/reset`, body, { headers: this.headers })
      .map((res: Response) => res.json())
      .toPromise()
      .then((res) => res)
      .catch((err) => {
        this.logged_in = false;
        return new Error(err);
      })
  }

  public async change(email, oldPass, newPass) {
    let token = await this.getToken();
    const body = {
      email: email,
      oldPass: oldPass,
      newPass: newPass
    };
    let headers: Headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${token}`);

    return this.http
      .post(`${this.baseUrl}/auth/change`, body, { headers: headers })
      .map((res: Response) => res.json())
      .toPromise()
      .then((res) => res)
      .catch((err) => {
        this.logged_in = false;
        return new Error(err);
      });
  }

  public signup(name, last_name, matricula, email, password, locale?: string, timezone?: string) {
    return this.http
      .post(`${this.baseUrl}/auth/register`,
        {
          name: name, last_name: last_name,
          matricula: matricula, email: email,
          password: password, locale: locale,
          timezone: timezone,
        },
        { headers: this.headers })
      .map((res: Response) => res.json())
      .toPromise()
      .then((res) => {
        this.setCredentials(res)
          .then(() => this.logged_in = true);
        return Promise.resolve(res);
      })
      .catch((err) => {
        this.logged_in = false
        return Promise.reject(err);
      });
  }

  public refresh() {
    let refresh_token = this.getRefresh_token();
    if (refresh_token) {
      let headers: Headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', `Bearer ${refresh_token}`);
      this.http
        .post(`${this.baseUrl}/auth/refresh`, '', {
          headers: headers,
        })
        .map((res: Response) => res.json())
        .subscribe(
          res => {
            this.updateTokens(res);
          },
          err => {
            this.logout();
          },
          () => {
          }
        );
    }

  }

  public logout() {
    let token = this.getToken();
    let body: any = {};
    this.local.remove(TOKEN_KEY);
    this.local.remove(EMAIL_KEY);
    this.local.remove(USER_NAME);
    this.local.remove(USER_LAST_NAME);
    this.local.remove(MATRICULA);
    this.local.remove(ROLE);
    this.zoneImpl.run(() => {
      this.email = null;
      this.profile_id = null;
      this.token = null;
      this.logged_in = false;
      this.user_name = null;
      this.user_last_name = null;
      this.matricula = null;
    });
    if (token) {
      let headers: Headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', `Bearer ${token}`);

      this.http
        .post(`${this.baseUrl}/auth/logout`, body, {
          headers: headers,
        })
        .map((res: Response) => res.json())
        .subscribe(
          res => {
            //console.log(res);
          },
          err => {
            console.log(err);
          },
          () => { }
        );
    }

    this.events.publish('auth:logout');
  }

  public isLoggedIn(): boolean {
    //return this.loggedIn;
    return (
      this.email != null &&
      this.email !== '' &&
      this.token != null &&
      this.token !== ''
    );
  }

  public isReady(): boolean {
    return this.ready;
  }

  public getEmail(): string {
    return this.email;
  }

  public getToken(): string {
    return this.token;
  }

  public getRefresh_token(): string {
    return this.refresh_token;
  }

  public getProfileId(): number {
    return parseInt(this.profile_id);
  }

  public getUserId(): number {
    return parseInt(this.user_id);
  }
}
