import { Injectable } from '@angular/core';
import { ServiceProvider } from '../service-provider/service-provider';
import { BaseModel, FlugQuery } from '../base-service/base-service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { LocalStorage } from '../../components/local-storage/local-storage';

export class Justificante extends BaseModel { }

@Injectable()
export class JustService extends ServiceProvider<Justificante> {
    protected endpoint: string = 'justificante';

    constructor(
        http: HttpClient,
        storage: LocalStorage
    ) {
        super(http, storage);
        this.setQuery({
            include: ['User']
        })
    }

    getPendingAdmin() {
        const query: FlugQuery = {
            where: {
                approved: null
            },
            sort: [['id', 'ASC']]
        }
        return this.http
            .get(`${this.getEndpoint()}/admin`, { params: this.addQuery(query) })
            .toPromise()
            .then((response: HttpResponse<Object>) => response)
            .catch(err => this.handleError(err));
    }

    getAdmin() {
        const query: FlugQuery = {
            where: {
                approved: { $ne: null }
            },
            sort: [['id', 'DESC']]
        }
        return this.http
            .get(`${this.getEndpoint()}/admin`, { params: this.addQuery(query) })
            .toPromise()
            .then((response: HttpResponse<Object>) => response)
            .catch(err => this.handleError(err));
    }
}
