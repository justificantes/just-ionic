import 'rxjs/add/operator/map';

import * as _ from 'underscore';
import 'rxjs/add/operator/toPromise';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { LocalStorage } from '../../components/local-storage/local-storage';

declare var justServer: any;

export interface FlugQuery {
  populate?: string | Array<any>;
  where?: any;
  sort?: any;
  skip?: number;
  limit?: number;
}

export abstract class BaseModel {
  id: number | string;

  // virtual, not saved to db
  dirty: boolean = false;
  [prop: string]: any;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }

  public isFilled(): boolean {
    return this.id != null && this.id != '';
  }

  public toJSON() {
    let copy = _.clone(this);
    Object.keys(this).forEach(key => {
      if (typeof this[key] == 'object' && copy[key]) {
        if ((copy[key] instanceof Date) || !copy[key].hasOwnProperty('id')) return;
        let newKey = `${key}_id`;
        copy[newKey] = this[key].id;
        delete copy[key];
      }
    });
    return copy;
  }
}

export abstract class BaseService {
  protected query: FlugQuery = {};
  protected headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  protected endpoint: string;
  protected serverAddress: string = `${justServer.protocol}://${justServer.url}${justServer.port}/api/v1`;
  protected adminPolicy: boolean = true;
  protected modelHasUserId: boolean = true;

  constructor(public http: HttpClient, public storage: LocalStorage) {
  }

  protected getEndpoint(): string {
    return `${this.serverAddress}/${this.endpoint}`;
  }

  protected addQuery(query: any = {}) {
    const qs: any = {};

    const opts = _.extendOwn(query, this.query);

    for (const key in opts) {
      if (typeof opts[key] === 'string') {
        qs[key] = opts[key];
      } else {
        qs[key] = JSON.stringify(opts[key]);
      }
    }

    return qs;
  }

  protected encodeForSubmit(item: any) {
    return JSON.stringify(item);
  }

  protected handleError(error: any): Promise<any> {
    if (error && error.status && error.error) {
      return Promise.reject(error.error);
    }
    if (error && error.status) {
      return Promise.reject(error.status);
    }
    return Promise.reject(error);
  }
}
