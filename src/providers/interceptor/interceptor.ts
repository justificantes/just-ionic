import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { fromPromise } from 'rxjs/observable/fromPromise';


import { Events } from 'ionic-angular';
import { LocalStorage } from '../../components/local-storage/local-storage';

@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {
  constructor(private storage: LocalStorage, private events: Events) {
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {

    if (req.url.match('/auth/login') || req.url.match('/auth/register')) {
      return next.handle(req);
    }

    const promise = this.storage.get('token').then((token) => {
      const authReq = req.clone({
        headers: req.headers.set('Authorization', `Bearer ${token}`),
      });

      return (next.handle(authReq).catch((error, caught) => {
        if (error.status === 401) {
          this.events.publish('auth:logout');
        }
        return Observable.throw(error);
      }) as any).toPromise();

    });

    return fromPromise(promise);

  }
}
