import { Injectable } from '@angular/core';
import { ServiceProvider } from '../service-provider/service-provider';
import { BaseModel, FlugQuery } from '../base-service/base-service';
import { HttpClient } from '@angular/common/http';
import { LocalStorage } from '../../components/local-storage/local-storage';
import { Observable } from 'rxjs/Observable';

export class Attachment extends BaseModel { }

@Injectable()
export class AttachmentService extends ServiceProvider<Attachment> {
    protected endpoint: string = 'attachment';

    constructor(
        http: HttpClient,
        storage: LocalStorage
    ) {
        super(http, storage);
    }

    uploadImage(image: File): Observable<any> {
        const formData = new FormData();
        formData.append('image', image);
        return this.http.post(`${this.getEndpoint()}/upload`, formData);
    }

    getImg(justid: number): Promise<File> {
        const query: FlugQuery = {
            where: {
                just_id: justid
            }
        }
        return this.http
            .get(`${this.getEndpoint()}`, { params: this.addQuery(query), responseType: 'blob' })
            // .map((res: Response) => res.blob())
            .toPromise()
            .then((response: any) => response)
            .catch(err => this.handleError(err));

    }
}
