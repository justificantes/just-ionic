import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, LoadingController } from 'ionic-angular';
import { AuthService } from '../../providers/auth/auth';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { validateEmail } from '../../components/email-validator/email-validator';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  loader: any;

  public form = new FormGroup({
    name: new FormControl('', Validators.compose([Validators.required, Validators.minLength(2)])),
    last_name: new FormControl('', Validators.compose([Validators.required, Validators.minLength(2)])),
    matricula: new FormControl('', Validators.compose([Validators.required, Validators.minLength(8)])),
    email: new FormControl('', Validators.compose([Validators.required, validateEmail])),
    password: new FormControl('', Validators.compose([Validators.required, Validators.minLength(8)])),
    passRepeat: new FormControl('', Validators.compose([Validators.required, Validators.minLength(8)])),
  });

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthService,
    public toastController: ToastController,
    public loadingController: LoadingController
  ) {
  }

  register() {
    if (this.form.value.password != this.form.value.passRepeat) {
      this.presentToast('-1');
    } else {
      this.presentLoader('Registrando usuario');
      this.auth.signup(
        this.form.value.name,
        this.form.value.last_name,
        this.form.value.matricula,
        this.form.value.email,
        this.form.value.password)
        .then((res) => {
          this.dismissLoader();
        })
        .catch((err) => {
          this.dismissLoader();
          this.presentToast(err.status);
        })
    }
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message == '0' ? "Comprueba tu conexión a internet." : "¡Las contraseñas no coinciden!",
      duration: 5000,
    });
    toast.present();
  }

  presentLoader(message: string) {
    this.loader = this.loadingController.create({
      content: message,
    });
    this.loader.present();
  }

  dismissLoader() {
    if (this.loader) this.loader.dismiss();
  }

}
