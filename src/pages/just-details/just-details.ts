import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { JustService } from '../../providers/justificante/justificante';
import { AttachmentService } from '../../providers/attachment/attachment';
import { AttachmentDetailPage } from '../attachment-detail/attachment-detail';

@IonicPage()
@Component({
  selector: 'page-just-details',
  templateUrl: 'just-details.html',
})
export class JustDetailsPage {
  loader: any;
  currentJust: any;

  justImg: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public justService: JustService,
    public toastController: ToastController,
    public loadingController: LoadingController,
    public attachmentService: AttachmentService
  ) {
    this.currentJust = this.navParams.get('justData');
  }

  ionViewWillLoad() {
    this.load();
  }

  imageToShow: any;

  async load() {
    try {
      const image = await this.attachmentService.getImg(this.currentJust.id);
      let reader = new FileReader();
      reader.addEventListener(
        "load",
        () => this.justImg = reader.result, false
      );
      if (image) {
        reader.readAsDataURL(image);
      }
    } catch (err) {
      console.error(err)
    }
  }

  set(input) {
    this.currentJust.approved = input;
    this.presentLoader(`${input == null ? "Marcando como pendiente" : input ? "Aprobando" : "Rechazando"}`)
    this.justService.update(this.currentJust)
      .then((res) => {
        setTimeout(() => {
          this.dismissLoader();
          this.navCtrl.pop()
        }, 500);
      })
      .catch((err) => {
        this.dismissLoader();
        this.presentToast("Comprueba tu conexión a internet.");
        console.error(err)
      })
  }

  attachmentDetail(){
    this.navCtrl.push(AttachmentDetailPage, {attachment: this.justImg})
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 5000,
    });
    toast.present();
  }

  presentLoader(message: string) {
    this.loader = this.loadingController.create({
      content: message,
    });
    this.loader.present();
  }

  dismissLoader() {
    if (this.loader) this.loader.dismiss();
  }

}
