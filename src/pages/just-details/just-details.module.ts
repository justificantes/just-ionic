import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JustDetailsPage } from './just-details';

@NgModule({
  declarations: [
    JustDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(JustDetailsPage),
  ],
})
export class JustDetailsPageModule {}
