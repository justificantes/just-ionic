import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { JustService, Justificante } from '../../providers/justificante/justificante';
import { JustDetailsPage } from '../just-details/just-details';

@IonicPage()
@Component({
  selector: 'page-admin-history',
  templateUrl: 'admin-history.html',
})
export class AdminHistoryPage {
  justificantes: Array<Justificante>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public justService: JustService,
  ) {
  }

  ionViewWillEnter() {
    this.load(null);
  }

  async load(refresher) {
    try {
      this.justificantes = await this.justService.getAdmin();
      if (refresher) refresher.complete();
    }
    catch (err) {
      console.error(err);
      if (refresher) refresher.complete();
    }
  }

  doRefresh(refresher) {
    this.load(refresher);
  }

  details(just) {
    this.navCtrl.push(JustDetailsPage, { justData: just });
  }

}
