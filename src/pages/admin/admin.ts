import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { AuthService } from '../../providers/auth/auth';
import { WelcomePage } from '../welcome/welcome';
import { AdminHistoryPage } from '../admin-history/admin-history';
import { AdminPendingPage } from '../admin-pending/admin-pending';

@IonicPage()
@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html',
})
export class AdminPage {

  constructor(
    public app: App,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public authService: AuthService
    ) {
  }

  logout() {
    this.authService.logout();
    this.app.getRootNav().setRoot(WelcomePage);
  }

  goAdminHistory() {
    this.navCtrl.push(AdminHistoryPage);
  }

  goAdminPending() {
    this.navCtrl.push(AdminPendingPage);
  }

}
