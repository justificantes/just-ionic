import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminPendingPage } from './admin-pending';

@NgModule({
  declarations: [
    AdminPendingPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminPendingPage),
  ],
})
export class AdminPendingPageModule {}
