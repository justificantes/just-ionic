import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { AuthService } from '../../providers/auth/auth';
import { WelcomePage } from '../welcome/welcome';
import { HistoryPage } from '../history/history';
import { SubmitPage } from '../submit/submit';

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  constructor(
    public app: App,
    public navCtrl: NavController,
    public navParams: NavParams,
    public authService: AuthService
  ) {
  }

  logout() {
    this.authService.logout();
    this.app.getRootNav().setRoot(WelcomePage);
  }

  goHistory() {
    this.navCtrl.push(HistoryPage);
  }

  goSubmit() {
    this.navCtrl.push(SubmitPage);
  }

}
