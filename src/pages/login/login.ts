import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { AuthService } from '../../providers/auth/auth';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { validateEmail } from '../../components/email-validator/email-validator';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loader: any;

  public form = new FormGroup({
    email: new FormControl('', Validators.compose([Validators.required, validateEmail])),
    password: new FormControl('', Validators.compose([Validators.required, Validators.minLength(8)])),
  });

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthService,
    public toastController: ToastController,
    public loadingController: LoadingController
  ) {
  }

  // "info@agiles.mx" "agilestest"
  login() {
    this.presentLoader('Iniciando sesión');
    this.auth.login(this.form.value.email, this.form.value.password)
      .then((res) => {
        this.dismissLoader();
      })
      .catch((err) => {
        this.dismissLoader();
        this.presentToast(err.status);
      });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message == '401' ? "Usuario y/o contraseña incorrecto." : "Comprueba tu conexión a internet.",
      duration: 5000,
    });
    toast.present();
  }

  presentLoader(message: string) {
    this.loader = this.loadingController.create({
      content: message,
    });
    this.loader.present();
  }

  dismissLoader() {
    if (this.loader) this.loader.dismiss();
  }

}
