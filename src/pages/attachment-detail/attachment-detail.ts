import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-attachment-detail',
  templateUrl: 'attachment-detail.html',
})
export class AttachmentDetailPage {
  justImg: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams) {
    this.justImg = this.navParams.get('attachment');
  }

}
