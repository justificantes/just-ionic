import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AttachmentDetailPage } from './attachment-detail';

@NgModule({
  declarations: [
    AttachmentDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(AttachmentDetailPage),
  ],
})
export class AttachmentDetailPageModule {}
