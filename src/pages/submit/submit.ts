import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, Platform, LoadingController, ToastController } from 'ionic-angular';
import { AuthService } from '../../providers/auth/auth';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AttachmentService, Attachment } from '../../providers/attachment/attachment';
import { JustService, Justificante } from '../../providers/justificante/justificante';
import * as moment from 'moment';


class ImageSnippet {
  pending: boolean = false;
  status: string = 'init';

  constructor(
    public src: string,
    public file: File
  ) { }
}

@IonicPage()
@Component({
  selector: 'page-submit',
  templateUrl: 'submit.html',
})
export class SubmitPage {
  loader: any;
  selectedFile: any;

  types = ['Médico', 'Personal', 'Otro'];

  public form = new FormGroup({
    type: new FormControl('', Validators.compose([Validators.required])),
    date_1: new FormControl(),
    date_2: new FormControl(),
    photo_id: new FormControl(),
    explanatory: new FormControl()
  });

  selected = {
    periodStart: moment().toISOString(true),
    periodEnd: moment().toISOString(true)
  };

  limit = {
    min: moment().subtract(1, 'month').toISOString(true),
    max: moment().toISOString(true)
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public authService: AuthService,
    public app: App,
    public justService: JustService,
    public attachmentService: AttachmentService,
    public platform: Platform,
  ) {
  }

  async submit() {
    this.presentLoader('Generando justificante');
    if (this.selectedFile != null) {
      this.attachmentService.uploadImage(this.selectedFile.file)
        .subscribe((res) => {
          this.createJust(res)
        }, (err) => {
          this.presentToast()
        })
    } else {
      this.createJust()
    }
  }

  createJust(imageName?) {
    const item = new Justificante({
      type: this.form.value.type,
      date_1: moment(this.form.value.date_1).startOf('day').utc().toDate(),
      date_2: moment(this.form.value.date_2).startOf('day').utc().toDate(),
      explanatory: this.form.value.explanatory,
    });

    this.justService.create(item)
      .then((res) => {
        if (imageName) {
          const attachment = new Attachment({
            file_name: imageName,
            just_id: res.id
          })
          this.attachmentService.create(attachment).then(() => {
            this.dismissLoader();
            this.navCtrl.pop();
          })
            .catch((err) => {
              this.presentToast();
            });
        } else {
          this.dismissLoader()
          this.navCtrl.pop();
        }
      })
      .catch((err) => {
        this.presentToast()
      });
  }

  async presentToast(message = 'Hubo un problema generando tu justificante.') {
    this.dismissLoader();
    const toast = await this.toastController.create({
      message: message,
      duration: 5000,
    });
    toast.present();
  }

  presentLoader(message: string) {
    this.loader = this.loadingController.create({
      content: message,
    });
    this.loader.present();
  }

  dismissLoader() {
    if (this.loader) this.loader.dismiss();
  }

  processFile(imageInput: any) {
    const file: File = imageInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('loadend', async (event: any) => {
      this.selectedFile = new ImageSnippet(event.target.result, file);
    });
    reader.readAsDataURL(file);
  }

}
